import java.util.*;
import java.util.regex.Pattern;


public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   Tnode (String a) {
      this.name = a;
      this.firstChild = null;
      this.nextSibling = null;
   }


   @Override
   public String toString() {
      StringBuilder b = new StringBuilder();

      b.append(name);
      if (firstChild != null) {
         b.append(String.format("(%s)", firstChild.toString()));
      }

      if (nextSibling != null) {
         b.append(String.format(",%s", nextSibling.toString()));
      }
      return b.toString();
   }

   public static Tnode buildFromRPN(String pol) {
      // https://www.baeldung.com/java-check-string-number - regex here

      Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

      if (pol == null || pol.trim().equals("")) {
         throw new RuntimeException("Tree node is empty!");
      }

      String[] splitString = pol.split(" ");
      LinkedList<Tnode> nodes = new LinkedList<>();

      for (String currentSymbol : splitString) {

         if (pattern.matcher(currentSymbol).matches()) {
            nodes.addFirst(new Tnode(currentSymbol));
            continue;
         }

         if (currentSymbol.equals("DUP")) {
            if (nodes.size() < 1) {
               throw new RuntimeException("Not enough trees to proceed operation!");
            }

            nodes.addFirst((Tnode) nodes.getFirst().clone());
            continue;
         }

         if (currentSymbol.equals("SWAP")) {
            if (nodes.size() < 2) {
               throw new RuntimeException("Not enough numbers in expression" + pol + "to execute SWAP operation.");
            }

            nodes.addLast(nodes.removeFirst());
            continue;
         }

         if (currentSymbol.equals("ROT")) {
            if (nodes.size() < 3) {
               throw new RuntimeException("Not enough numbers in expression" + pol + "to execute ROT operation.");
            }

            nodes.addFirst(nodes.removeLast());
            continue;
         }

         List<String> operators = Arrays.asList("+", "-", "/", "*");
         if (!operators.contains(currentSymbol)) {
            throw new IllegalArgumentException("Illegal symbol " + currentSymbol + " in expression.");
         }

         Tnode currentRoot = new Tnode(currentSymbol);

         if (nodes.size() < 2) {
            throw new IllegalArgumentException("Too few numbers in expression " + pol);
         }

         Tnode nextSibling = nodes.removeFirst();
         Tnode firstChild = nodes.removeFirst();

         currentRoot.firstChild = firstChild;
         firstChild.nextSibling = nextSibling;


         nodes.addFirst(currentRoot);
      }

      if (nodes.size() > 1) {
         throw new IllegalArgumentException("Too many numbers in expression " + pol);
      }

      return nodes.removeLast();
   }

   @Override
   public Object clone() {

      Tnode clone = new Tnode(name);

      if (firstChild != null) {
         clone.firstChild = (Tnode) firstChild.clone();
      }

      if (nextSibling != null) {
         clone.nextSibling = (Tnode) nextSibling.clone();
      }

      return clone;
   }

   public static void main(String[] param) {
      String a = "2 1 - 4 * 6 3 / +";
      Tnode t = buildFromRPN(a);
      System.out.println("------");
      System.out.println(t);
      String rpn = "1 2 +";
      System.out.println("RPN: " + rpn);
      Tnode res = buildFromRPN(rpn);
      System.out.println("Tree: " + res);
   }
}

// https://gist.github.com/vo/2481737
